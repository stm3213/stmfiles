# Micro-controllers - ARM

## Agenda
* Watchdog Timer
* ADC
* DAC
* Brown-out detector

## Watchdog Timer
* Downcounter -- If clears, reset the processor.
* Used to ensure that tasks are completed in well-defined time -- otherwise system reset (so that error can be reported).
* STM32 WDT
	* Independent WDT
		* Reset on timeout
		* Separate clock (LSI)
		* Less timing accuracy
	* Window WDT
		* Reset on timeout
		* Can produce interrupt on timeout
		* APB1 -- prescaled
		* More timing accuracy
* Independent WDT
	* 125 us to 32.8 secs -- time period
	* Programming sequence
		* Refer slides/user manual

## ADC
* Analog to Digital converter
* Most of sensors (e.g. temperature, LDR, Pressure, pH, etc.) are analog.
* Types of ADC
	* "Successive Approximation" ADC -- most used in micro-controllers
	* Dual slope ADC
	* Flash 
* ADC characteristics
	* Resolution --> Number of bits
		* e.g. 8-bit ADC --> Number of steps = 256
	* Reference Voltage --> Analog input is compared against Vref.
		* If only +ve voltage is allowed, then Max Vin = Vref.
		* e.g. Vref = 2.56 V 
	* Conversion Time
		* ADC clock freq (Fadc)
		* Successive Approx ADC -- Number of clocks = Resolution + 1.
			* If Resolution is 8-bits, then Number of clocks 9.
			* e.g. conversion = 9 / Fadc.
	* Step size
		* Minimum voltage change that can be detected by ADC.
		* Vref / steps
		* e.g. 2.56 V / 256 = 0.01 V = 10 mV
* ADC Formuala
	* Reading = Vin / Step Size
	* Reading = Vin / (Vref / steps)
	* e.g. Vin = 1 V, Then Digital Reading = 1 / 0.01 = 100 

### STM32 ADC
* The 12-bit ADC is a successive approximation analog-to-digital converter.
	* 12-bit, 10-bit, 8-bit or 6-bit configurable resolution
* ADC1, ADC2, ADC3.
* ADC conversion modes
	* single - Single conversion at a time.
		* Start ADC, Get reading, Stop ADC.
		* Slow applications (NO time critical reading)
	* continuous -- Convert at highest possible speed (as per Prescalar setting).
		* Start ADC, Keep getting readings (loop), Stop ADC
		* Also called as BURST mode.
		* Can use interrupt -- ADC conversion completion.
	* scan -- Reading from multiple channels (multiple sensor)
		* Get readings from individual sensors.
	* discontinuous
		* Refer manual.
* ADC input range: VREF– ≤ VIN ≤ VREF+
* Conversion rate: 2.4M per sec (max)

* ADC Clock
	* ADC --> APB 2
	* Fapb2 = 16 MHz (Set HSI and Prescalars)
	* Set ADC Prescalar = 8
	* ADC clock = 2 MHz

* ADC Sample Rate
	* Useful in continuous mode to control sample/conversion time.
	* ADC clock = 2 MHz
	* Set Sampling Time = 480 (cycles)
	* Conversion Time for 12-bit value (cycles) = Sampling Time + 12 cycles
		* Conversion Time - Tconv = 480 + 12 = 492 cycles
	* Conversion Time (sec) = Conversion cycles / ADC clock
		* 492 / 2000000 = 246 usec

## DAC
* Digital to Analog Converter
* Like PWM
	* PWM circuit is digital.
	* DAC circuit is more complex, but more accurate.
* DAC characteristics
	* Resolution: e.g. 8-bit --> 256 steps
	* Max Vout = Vref = Vdd (e.g. Vdd 3V)
	* Step size = Vout / steps
		* = 3 V / 256 = 11.7 mV
		* A small change in digital input will cause 11.7 mV change in output.
	* Vout = Din * Step size
		* If Din = 0000 0000 --> Vout = 0
		* If Din = 0000 0001 --> Vout = Din * Step Size = 1 * 11.7 = 11.7 mV
		* If Din = 0000 1010 --> Vout = Din * Step Size = 10 * 11.7 = 117 mV
		* If Din = 1111 1111 --> Vout = Din * Step Size = 255 * 11.7 = 3 V

### STM32 DAC
* Single DAC 12-bit (Can be configured as 8-bit or 12-bit).
* Two output channels
* Digital input can be left/right aligned in DR.

### Brown Out Detector
* BOD is used to detect drop in Vdd level.
* Voltage level is reduced due to
	* Drop in supply voltage (power supply).
	* Browning of the components.
* 













# Micro-controllers - ARM

## Agenda
* STM32 Timers
	* Time-base with polling & interrupt
	* Output capture
	* PWM
* Watchdog Timer

## Timing Formula
* N = (Fpclk / 1000) * t / PR
	* t is time to be measured in ms.
	* Fpclk is timer peripheral clock.

## STM32 Timers
* STM32 Timers can work in variety of modes/features.
	* Time base generation (delay/periodic interrupt)
	* External clock counter
	* Input capture
	* Output compare
	* Pulse Width Modulation
	* One pulse mode
	* Encoder mode
	* Infra-red mode
* STM32 has different types of Timers.
	* Basic timers (TIM6 and TIM7)
		* 16-bit timers (0 - 65535)
		* Only time-base generation
		* No compare/capture channel
	* General purpose timers
		* 16-bit (TIM3 and TIM4) and 32-bit timers (refer docs)
		* Time-base generation
		* Compare/capture channels
		* PWM mode, Infra-red mode, Encoder mode, One pulse mode, ...
	* Advanced control timers (TIM1 and TIM8)
		* 32-bit timers
		* Time-base generation
		* Compare/capture channels
		* PWM mode, Infra-red mode, Encoder mode, One pulse mode, ...
		* Advanced controls: PWM brake (stop), ...

### Time-base generation
* Basic timer -- TIM6
* Desired delay t = 10000 ms
* Fpclk = 12.5 MHz
* Prescalar = 12500 (-1)
* Number of clocks N = (Fpclk / 1000) * t / PR = 10000 (-1)

#### TIM6 Polling Demo

#### TIM6 Interrupt Demo

### Output Compare

#### TIM4 Output Compare Demo

### Input Capture

### PWM
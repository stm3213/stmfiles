# Advanced Micro-controllers - ARM

## Agenda
* RTC Timer (Wall/Calendar time)
* SysTick Timer
* Timers
	* Basic Timers
	* General purpose Timers
	* Special Timers
* PWM
* Watchdog Timer

## UART
* UART programming
	* UART Registers -- Config, Baud, Polling, Interrupts -- Putty on 
	* Using ST HAL
* UART 2
	* PA2 (TX), PA3 (RX)
	* RS-232 dongle ---> STM 32
		* TX --> RX
		* RX --> TX

## Time Manangement
* Timing requirements in any system
	* Absolute time (Wall time)
	* Relative time
* Absolute time
	* Get calendar + clock time.
	* Display date & time, alarm set, etc.
* Relative time
	* To get duration between two events (e.g. two ADC readings, two external interrupts, ...)
	* Schedule a task after certain duration
	* Delay generation
	* Periodic interrupts

### RTC
* Provides Absolute time in the system (calendar + clock time).
* Typically RTC is connected to CMOS battery and keep functioning even if processor is off.

#### STM32 RTC
* The real-time clock (RTC) is an independent "BCD" timer/counter. 
	* 4:15:30
		* Binary	: 0000 0100 : 0000 1111 : 0001 1110
		* BCD (0-9)	: 0000 0100 : 0001 0101 : 0011 0000
* Time + Calendar Date
* Two alarms (Max 1 month) with Interrupt
* Periodic wakeup Interrupt
* Two 32-bit registers for date and time for time-keeping.
	* RTC_TR -- Time Register
	* RTC_DR -- Date Register
* Auto management of Leap year and Daylight saving.
* Year range is 2000 to next 100 years.
* Additional registers for Alarm setting.
* As long as the supply voltage remains in the operating range, the RTC never stops, regardless of the device status (Run mode, low-power mode or under reset).
* RTC clock source -- Need stable clock 1 Hz.
	* LSI -- 32 KHz (RC-osc -- less stable)
	* LSE -- 32768 Hz (Xtal -- stable)
	* HSE / 32 -- Xtal
* RTC clock (32768 Hz) --> Async Prescalar (128) --> 256 Hz --> Sync Prescalar (256) --> 1 Hz.
* RTC Config Registers

### Timer (General)
* Timer is peripheral.
* Timer vs Counter
	* Counter is typically used to count pulses (at variable rate).
	* Counter types: Up counter (0 to MAX) and Down counter (MAX to 0).
	* Timer is a counter (circuit) that counts pulses/edges with fixed frequency.
	* Usually timer hardware produces interrupt on certain event compare, overflow, etc.
	* Timer is used measure time or frequency of input signal.
* Clock (1 Hz) --> Timer/Counter --> cnt
	* Incremented per second.
	* If count = 100, then time = 100 sec.
* Clock (1000 Hz) --> Timer/Counter --> cnt
	* 1000 pulses per second.
	* If count = 5000, then time = 5 sec.
* F = pulses / sec
* Period of one clock T = 1 / F
* Number of clocks "N" : time (t) = N * T = N / F
* To measure "t" seconds, how many clocks are required?
	* N = t * F

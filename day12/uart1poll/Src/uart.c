/*
 * uart.c
 *
 *  Created on: 07-Jul-2021
 *      Author: Nilesh Ghule <nilesh@sunbeaminfo.com>
 */

#include "uart.h"

void UartInit(uint32_t baud) {
	// enable clock for GPIOA
	RCC->AHB1ENR |= BV(RCC_AHB1ENR_GPIOAEN_Pos);
	// set alt function for PA.2 & PA.3 as USART pins
	USART_AFR |= (GPIOA_USART_ALT_FN << GPIO_AFRL_AFSEL3_Pos) | (GPIOA_USART_ALT_FN << GPIO_AFRL_AFSEL2_Pos);
	// set gpio pin mode as Alt Fn [10]
	USART_GPIO->MODER &= ~(BV(USART_TX_PIN*2) | BV(USART_RX_PIN*2));
	USART_GPIO->MODER |= BV(USART_TX_PIN*2 + 1) | BV(USART_RX_PIN*2 + 1);
	// set speed of gpio pin [00]
	USART_GPIO->OSPEEDR &= ~(BV(USART_TX_PIN*2+1) | BV(USART_TX_PIN*2) | BV(USART_RX_PIN*2+1) | BV(USART_RX_PIN*2));
	// set push-mode output type [0]
	USART_GPIO->OTYPER &= ~(BV(USART_TX_PIN) | BV(USART_RX_PIN));
	// set no pull-up and pull-dwon resistor
	USART_GPIO->PUPDR &= ~(BV(USART_TX_PIN*2+1) | BV(USART_TX_PIN*2) | BV(USART_RX_PIN*2+1) | BV(USART_RX_PIN*2));

	// enable usart2 clock
	RCC->APB1ENR |= BV(RCC_APB1ENR_USART2EN_Pos);

	// usart2 config -- control register (CR1, CR2 & CR3)
	// enable uart tx (TE=1), rx (RE=1), word len 8 (M=0), parity off (PCE=0), oversampling by 16 (OVER8=0)
	USART->CR1 = BV(USART_CR1_TE_Pos) | BV(USART_CR1_RE_Pos);
	// one stop bit(STOP=00), disable clock (uart not usart)
	USART->CR2 = 0;
	// no hardware flow control (RTSE=0, CTSE=0), no dma, no interrupts enabled
	USART->CR3 = 0;
	// configure baud rate
	if(baud == BAUD_9600)
		USART->BRR = 0x683;
	else if(baud == BAUD_115200)
		USART->BRR = 0x8B;
	// enable uart
	USART->CR1 |= BV(USART_CR1_UE_Pos);
}

void UartPutch(int ch) {
	// wait for last char transmitted
	while((USART->SR & BV(USART_SR_TXE_Pos)) == 0);
	// write char into data register
	USART->DR = ch & 0x000000FF;
}

int UartGetch(void) {
	int ch;
	// wait for char to be received
	while((USART->SR & BV(USART_SR_RXNE_Pos)) == 0);
	// read char from data register
	ch = USART->DR;
	// return the character read
	return ch;
}

void UartPuts(char *str) {
	int i;
	for(i=0; str[i]!='\0'; i++)
		UartPutch(str[i]);
}

void UartGets(char *str) {
	int i=0, ch;
	do {
		ch = UartGetch();
		str[i++] = ch;
	}while(ch != '\r');
	str[i++] = '\n';
	str[i] = '\0';
}


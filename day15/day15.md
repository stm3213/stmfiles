# ARM Cortex-M

## Legacy GPIO vs Fast GPIO in ARM-7 (LPC-2148)
* Fast GPIO -- AHB
* Legacy GPIO -- APB
* Fast GPIO -- Cclk (Fast)
* Legacy GPIO -- Pclk (Slow)
* Fast GPIO -- GPIO registers are Word, Half-word and Byte accessible
* Legacy GPIO -- GPIO registers are Word accessible
* Fast GPIO -- Mask register is available
* Legacy GPIO -- Mask register is not available

## LCD - HD44780
* HD44780
	* MPU interface
		* 4-bit: DB4 to DB7
		* 8-bit: DB0 to DB7
		* Busy Flag: DB7 (read)
			* 1: LCD is busy (in executing last Instruction)
			* 0: LCD is available
		* RS: Register Select
			* Select between Instruction Register (0) or Data Register (1)
		* RW: Read/Write
			* Read(1) or Write(0)
		* EN: Enable
			* Data on data lines will be latched (into LCD) when there is falling edge on EN.
	* DDRAM
		* Line1 address: 00
		* Line2 address: 40
* To write instruction (4-bit mode)
	* RS=0
	* RW=0
	* Send high nibble (of instruction)
	* Falling edge on EN
	* Send lower nibble (of instruction)
	* Falling edge on EN
	* Wait for busy flag
* To write data/ascii (4-bit mode)
	* RS=1
	* RW=0
	* Send high nibble (of data)
	* Falling edge on EN
	* Send lower nibble (of data)
	* Falling edge on EN
	* Wait for busy flag
* Wait for busy flag
	* RS=0
	* RW=1
	* EN=1
	* Read data
	* Repeat read if DB7=1
	* EN=0
* When reset, by default HD44780 does following:
	* Clear display
	* Function set: 8-bit mode, single line display, 5x8 font
	* Display control: Display off, Cursor off, Blinking off
	* Entry mode: Addr Incr, No shift
* HD44780 instructions (for our cct)
	* Clear display: 0x01
	* Entry mode: Addr Incr, No shift: 0x06
	* Display control: Display on, Cursor off, Blinking off: 0x0C
	* Function set: 4-bit mode, 2-line, 5x8: 0x28
	* Line1 DDRAM Address: 0x80
	* Line2 DDRAM Address: 0xC0
* BlueBoard HD44780 connections
	* 4-bit mode
	* LCD_RS --> ~~P2.16~~	--> P1.24
	* LCD_RW --> ~~P3.4~~	--> P1.23
	* LCD_EN --> ~~P3.8~~	--> P1.22
	* LCD_D4 --> ~~P3.3~~	--> P2.4
	* LCD_D5 --> ~~P3.5~~	--> P2.5
	* LCD_D6 --> ~~P3.6~~	--> P2.6
	* LCD_D7 --> ~~P3.7~~	--> P2.7


## Pending topics
* RTC
* Timers
* PWM
* SysTick
* Watchdog
* CAN
* ADC

/*
 * eeprom.h
 *
 *  Created on: Jul 10, 2021
 *      Author: Nilesh Ghule <nilesh@sunbeaminfo.com>
 */

#ifndef INC_EEPROM_H_
#define INC_EEPROM_H_


#include "stm32f407xx.h"
#include "stm32f4xx_hal.h"
#include <stdint.h>
#include <stdbool.h>

#define EEPROM_DEV_ADDR	0xA0

bool eeprom_init(I2C_HandleTypeDef *hi2c);
bool eeprom_write(uint16_t addr, uint8_t byte[], uint8_t n);
bool eeprom_read(uint16_t addr, uint8_t byte[], uint8_t n);


#endif /* INC_EEPROM_H_ */

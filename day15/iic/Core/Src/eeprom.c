/*
 * eeprom.c
 *
 *  Created on: Jul 10, 2021
 *      Author: Nilesh Ghule <nilesh@sunbeaminfo.com>
 */

#include "eeprom.h"

static I2C_HandleTypeDef hi2cEeprom;
static bool eepromReady;

bool eeprom_init(I2C_HandleTypeDef *hi2c) {
	if(HAL_I2C_IsDeviceReady(hi2c, EEPROM_DEV_ADDR, 2, 10) == HAL_OK) {
		hi2cEeprom = *hi2c;
		eepromReady = true;
	} else
		eepromReady = false;
	return eepromReady;
}

bool eeprom_write(uint16_t addr, uint8_t byte[], uint8_t n) {
	if(!eepromReady)
		return false;
	if(HAL_I2C_Mem_Write(&hi2cEeprom, EEPROM_DEV_ADDR, addr, 2, byte, n, 100) == HAL_OK)
		return true;
	return false;
}

bool eeprom_read(uint16_t addr, uint8_t byte[], uint8_t n) {
	if(!eepromReady)
		return false;
	if(HAL_I2C_Mem_Read(&hi2cEeprom, EEPROM_DEV_ADDR, addr, 2, byte, n, 100) == HAL_OK)
		return true;
	return false;
}


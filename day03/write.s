.equ SYS_EXIT, 1
.equ SYS_WRITE, 4
.equ STDOUT, 1

.data
.balign 4
	str:
		.ascii "Hello World\n\0"

.text
.balign 4
.globl main
.func main
main:

	@write(stdout, "Hello World\n", 12);
	mov r7, #SYS_WRITE
	mov r0, #STDOUT
	ldr r1, =str
	mov r2, #12
	swi 0

	@_exit(0);
	mov r7, #SYS_EXIT
	mov r0, #0
	swi 0

	bx lr
.endfunc



.equ SYS_EXIT, 1

.globl main
.func main
main:

	@ _exit(0);
	mov r7, #SYS_EXIT
	mov r0, #0
	swi 0

	bx lr
.endfunc






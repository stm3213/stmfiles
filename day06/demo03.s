/* *************************************************************************
**  Target      : Cortex-M3
**  Environment : GNU Tools
**  Micro-Controller : LM3SXXXX *************************************************************************
*/
.syntax unified

.global pfnVectors
.global Default_Handler
.global _start
.equ Top_Of_Stack, 0x20004000    /* end of 16K RAM */

.global sdata
.global edata
.global etext
.global sbss
.global ebss

.section .vectors
vectors:
	.word Top_Of_Stack          @ msp = 0x20004000
	.word _start                @ Starting Program address
	.word Default_Handler	    @ NMI_Handler
	.word Default_Handler	    @ HardFault_Handler
	.word Default_Handler	    @ MemManage_Handler
	.word Default_Handler	    @ BusFault_Handler
	.word Default_Handler	    @ UsageFault_Handler
	.word 0                     @ 7
	.word 0                     @ To
	.word 0                     @ 10 
	.word 0                     @ Reserved
	.word Default_Handler	    @ SVC_Handler
	.word Default_Handler	    @ DebugMon_Handler
	.word 0                     @ Reserved
	.word Default_Handler	    @ PendSV_Handler
	.word Default_Handler	    @ SysTick_Handler
	.word Default_Handler	    @ IRQ_Handler
	.word Default_Handler	    @ IRQ_Handler

.section .rodata


.section .data


.section .bss


.section .text
.thumb
/**
 * This is the code that gets called when the processor first
 * starts execution following a reset. 
*/
.type _start, %function
_start:                              @ _start label is required by the linker

	//init .data section
	ldr r7, =etext
	ldr r6, =sdata
	ldr r5, =edata
	mov r4, #0
data_init:
	cmp r6, r5
	beq data_init_end
	ldrb r4, [r7], #1
	strb r4, [r6], #1
	b data_init
data_init_end:

	//clear .bss section
	ldr r6, =sbss
	ldr r7, =ebss
	mov r4, #0
bss_init:
	cmp r6, r7
	beq bss_init_end
	strb r4, [r6], #1
	b bss_init
bss_init_end:

	bl main
	stop:   b stop

/***************************************************************/
.global main
.type main, %function
main:   
	@ thumb instruction set
	@ copy max of r1 and r2 into r0 register.
	mov r1, #12		@ r1 = 12
	mov r2, #10		@ r2 = 10

	cmp r1, r2		@ r1 - r2 --> update status flags (NZCV)
	ble max2 		@ if(r1 <= r2)	goto max2	(conditional jump)
max1:
	mov r0, r1		@ r0 = r1
	b maxend		@ goto maxend	(unconditional jump)
max2:				
	mov r0, r2		@ r0 = r2
maxend:

	/*
	@ arm instruction set (not allowed in thumb/thumb-2) -- conditional execution of instructions
	@ copy max of r1 and r2 into r0 register.
	mov r1, #12		@ r1 = 12
	mov r2, #10		@ r2 = 10

	cmp r1, r2		@  r1 - r2 --> update status flags (NZCV)
	movle r0, r2	@ if(r1 <= r2) 	r0 = r2;
	movgt r0, r1	@ if(r1 > r2)	r0 = r1;
	*/

	@ thumb2 instruction set -- conditional execution using if-then instruction
	mov r1, #12		@ r1 = 12
	mov r2, #15		@ r2 = 15

	cmp r1, r2		@  r1 - r2 --> update status flags (NZCV)
	ite le			@ if(r1 <= r2)
	movle r0, r2	@	r0 = r2
	movgt r0, r1 	@ else
					@ 	r0 = r1

	/*
	IT instruction in thumb2 is used for conditional execution of max 4 instructions following it.
	ITE --> If Then Else
		--> Instruction 1 after IT execute if condition is True (Then).
		--> Instruction 2 after IT execute if condition is False (Else).

	ITTEE --> If Then Then Else Else
		--> Instruction 1 after IT execute if condition is True (Then).
		--> Instruction 2 after IT execute if condition is True (Then).
		--> Instruction 3 after IT execute if condition is False (Else).
		--> Instruction 4 after IT execute if condition is False (Else).

	if(r3 == r4) {
		r0 = r3;
		r1 = r4;
	} else {
		r0 = r4;
		r1 = r3;
	}

	cmp r3, r4
	ittee eq
	moveq r0, r3
	moveq r1, r4
	movne r0, r4
	movne r1, r3

	if(r3 == r4) {
		r0 = r3;
		r1 = r4;
		r2 = r3;
	} else {
		r0 = r4;
	}

	cmp r3, r4
	ittte eq
	moveq r0, r3
	moveq r1, r4
	moveq r2, r3
	movne r0, r4

	*/
	mov pc, lr
/***************************************************************/

/**
 * This is the code that gets called when the processor receives an
 * unexpected interrupt.  This simply enters an infinite loop,preserving
 * the system state for examination by a debugger.
 *
*/

.type Default_Handler, %function
Default_Handler:
	halt:
		b halt
.end


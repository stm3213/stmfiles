/* *************************************************************************
**  Target      : Cortex-M3
**  Environment : GNU Tools
**  Micro-Controller : LM3SXXXX *************************************************************************
*/
.syntax unified

.global pfnVectors
.global Default_Handler
.global _start
.equ Top_Of_Stack, 0x20004000    /* end of 16K RAM */

.global sdata
.global edata
.global etext
.global sbss
.global ebss

.section .vectors
vectors:
	.word Top_Of_Stack          @ msp = 0x20004000
	.word _start                @ Starting Program address
	.word Default_Handler	    @ NMI_Handler
	.word Default_Handler	    @ HardFault_Handler
	.word Default_Handler	    @ MemManage_Handler
	.word Default_Handler	    @ BusFault_Handler
	.word Default_Handler	    @ UsageFault_Handler
	.word 0                     @ 7
	.word 0                     @ To
	.word 0                     @ 10 
	.word 0                     @ Reserved
	.word Default_Handler	    @ SVC_Handler
	.word Default_Handler	    @ DebugMon_Handler
	.word 0                     @ Reserved
	.word Default_Handler	    @ PendSV_Handler
	.word Default_Handler	    @ SysTick_Handler
	.word Default_Handler	    @ IRQ_Handler
	.word Default_Handler	    @ IRQ_Handler

.section .rodata


.section .data


.section .bss


.section .text
.thumb
/**
 * This is the code that gets called when the processor first
 * starts execution following a reset. 
*/
.type _start, %function
_start:                              @ _start label is required by the linker

	//init .data section
	ldr r7, =etext
	ldr r6, =sdata
	ldr r5, =edata
	mov r4, #0
data_init:
	cmp r6, r5
	beq data_init_end
	ldrb r4, [r7], #1
	strb r4, [r6], #1
	b data_init
data_init_end:

	//clear .bss section
	ldr r6, =sbss
	ldr r7, =ebss
	mov r4, #0
bss_init:
	cmp r6, r7
	beq bss_init_end
	strb r4, [r6], #1
	b bss_init
bss_init_end:

	bl main
	stop:   b stop

/***************************************************************/
.global main
.type main, %function
main:                              
	@ collatz conjure r1=5 --> get all terms in r0 (one by one)
	@	if num % 2 != 0, then num = 3 * num + 1
	@	if num % 2 == 0, then num = num / 2
	@	if num & 1 != 0, then num = (num << 1) + num + 1
	@	if num & 1 == 0, then num = num >> 1

	mov r1, #5				@ r1 = 5
	mov r0, r1				@ r0 = r1
loop:
	cmp r0, #1				@ if(r0 == 1)
	beq loopend				@	goto loopend

	tst r0, #1				@ if((r0 & 1) == 0) --> if num is even, AND result = 0 (Z = 1)
	beq even				@	goto even
odd:
	mov r2, r0, lsl #1		@ r2 = r0 << 1		(inline barrel shifter -- 2nd arg)
	add r0, r2				@ r0 = r0 + r2
	add r0, #1				@ r0 = r0 + 1
	b loop					@ goto loop
even:
	@mov r0, r0, lsr #1		@ r0 = r0 >> 1
	lsr r0, #1				@ r0 = r0 >> 1		(barrel shifter)
	b loop					@ goto loop
loopend:

	mov pc, lr
/***************************************************************/

/**
 * This is the code that gets called when the processor receives an
 * unexpected interrupt.  This simply enters an infinite loop,preserving
 * the system state for examination by a debugger.
 *
*/

.type Default_Handler, %function
Default_Handler:
	halt:
		b halt
.end


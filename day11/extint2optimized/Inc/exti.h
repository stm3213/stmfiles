/*
 * exti.h
 *
 *  Created on: 06-Jul-2021
 *      Author: Nilesh Ghule <nilesh@sunbeaminfo.com>
 */

#ifndef EXTI_H_
#define EXTI_H_

#include "stm32f4xx.h"

// ext intr 0 flag declaration
extern volatile int exti0_flag;

#define SWITCH_EXTI			0
#define SWITCH_EXTI_IRQn	EXTI0_IRQn		/*EXTI0_IRQn = 6 (cmsis)*/

#define SWITCH_CLK_EN	RCC_AHB1ENR_GPIOAEN_Pos
#define SWITCH_GPIO		GPIOA
#define SWITCH_PIN		0

// SWITCH_EXTICR[3:0] = 0000
#define SWITCH_EXTICR	SYSCFG->EXTICR[0]
#define SWICTH_EXTICR_Msk	(BV(3) | BV(2) | BV(1) | BV(0))

void SwitchExtiInit(void);

#endif /* EXTI_H_ */






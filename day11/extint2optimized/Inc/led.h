/*
 * led.h
 *
 *  Created on: Jul 5, 2021
 *      Author: admin
 */

#ifndef LED_H_
#define LED_H_

#include "stm32f4xx.h"

#define LED_GPIO		GPIOD
#define LED_EN_CLOCK	3

#define LED_GREEN_PIN	12
#define LED_ORANGE_PIN	13
#define LED_RED_PIN		14
#define LED_BLUE_PIN	15

void LedInit(uint32_t pin);
void LedOn(uint32_t pin);
void LedOff(uint32_t pin);
void LedToggle(uint32_t pin);
void LedBlink(uint32_t pin, uint32_t ms);

#endif /* LED_H_ */

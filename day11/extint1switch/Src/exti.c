/*
 * exti.c
 *
 *  Created on: 06-Jul-2021
 *      Author: Nilesh Ghule <nilesh@sunbeaminfo.com>
 */
#include "exti.h"
#include "led.h"

void SwitchExtiInit(void) {
	// config gpio (PA.0) for switch
	// enable gpio A clock
	RCC->AHB1ENR |= BV(SWITCH_CLK_EN);

	// set switch pins as input pins [00]
	SWITCH_GPIO->MODER &= ~(BV(SWITCH_PIN * 2 + 1) | BV(SWITCH_PIN * 2));

	// set low speed [00]
	SWITCH_GPIO->OSPEEDR &= ~(BV(SWITCH_PIN * 2 + 1) | BV(SWITCH_PIN * 2));

	// configure no pull-up & pull-down resistor
	SWITCH_GPIO->PUPDR &= ~(BV(SWITCH_PIN * 2 + 1) | BV(SWITCH_PIN * 2));

	// syscfg exti config --> PA0 --> EXTI0
	// enable clock for syscfg
	RCC->APB2ENR |= BV(RCC_APB2ENR_SYSCFGEN_Pos);	//RCC_APB2ENR_SYSCFGEN_Pos = 14
	// SWITCH_EXTICR[3:0] = 0000 (Port A)
	SWITCH_EXTICR &= ~SWICTH_EXTICR_Msk;

	// ext intr controller config
	// config gpio line for falling edge (switch release)
	EXTI->FTSR |= BV(SWITCH_EXTI);
	EXTI->RTSR &= ~BV(SWITCH_EXTI);

	// unmask exti0 from IMR
	EXTI->IMR |= BV(SWITCH_EXTI);

	// ext intr NVIC config
	// enable intr into ISER
	NVIC_EnableIRQ(SWITCH_EXTI_IRQn);
}

// define EXTI0 handler function
void EXTI0_IRQHandler(void) {
	// acknowlege the interrupt
	EXTI->PR |= BV(SWITCH_EXTI);

	// interrupt handling logic
	LedBlink(LED_BLUE_PIN, 1000);
}



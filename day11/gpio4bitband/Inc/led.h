/*
 * led.h
 *
 *  Created on: 06-Jul-2021
 *      Author: Nilesh Ghule <nilesh@sunbeaminfo.com>
 */

#ifndef LED_H_
#define LED_H_

#include "stm32f4xx.h"

#define PERI_BB			0x40000000
#define PERI_BB_ALS		0x42000000

//#define PERI_BB_ALS_ADDR(regr,bit)	(PERI_BB_ALS + ((((uint32_t)&(regr))-PERI_BB) * 32) + ((bit)*4))
#define PERI_BB_ALS_ADDR(regr,bit)	(PERI_BB_ALS + ((((uint32_t)&(regr))-PERI_BB) << 5) + ((bit) << 2))
#define PERI_BB_ALS_WORD(regr,bit)	(*(uint32_t*)PERI_BB_ALS_ADDR(regr,bit))

#define LED_GPIO		GPIOD
#define LED_EN_CLOCK	3

#define LED_GREEN_PIN	12
#define LED_ORANGE_PIN	13
#define LED_RED_PIN		14
#define LED_BLUE_PIN	15

void LedInit(uint32_t pin);
void LedOn(uint32_t pin);
void LedOff(uint32_t pin);
void LedToggle(uint32_t pin);
void LedBlink(uint32_t pin, uint32_t ms);

#endif /* LED_H_ */

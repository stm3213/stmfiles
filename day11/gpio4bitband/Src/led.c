/*
 * led.c
 *
 *  Created on: 06-Jul-2021
 *      Author: Nilesh Ghule <nilesh@sunbeaminfo.com>
 */


#include "led.h"

void LedInit(uint32_t pin) {
	// enable gpio D clock
	RCC->AHB1ENR |= BV(LED_EN_CLOCK);

	// set all led pins as output pins [01]
	//LED_GPIO->MODER &= ~BV(pin * 2 + 1);
	//LED_GPIO->MODER |= BV(pin * 2);
	PERI_BB_ALS_WORD(LED_GPIO->MODER, pin * 2 + 1) = 0;
	PERI_BB_ALS_WORD(LED_GPIO->MODER, pin * 2) = 1;


	// set low speed [00]
	//LED_GPIO->OSPEEDR &= ~(BV(pin * 2 + 1) | BV(pin * 2));
	PERI_BB_ALS_WORD(LED_GPIO->OSPEEDR, pin * 2 + 1) = 0;
	PERI_BB_ALS_WORD(LED_GPIO->OSPEEDR, pin * 2) = 0;

	// configure output type to push-pull
	//LED_GPIO->OTYPER &= ~BV(pin);
	PERI_BB_ALS_WORD(LED_GPIO->OTYPER, pin) = 0;

	// configure no pull-up & pull-down resistor
	//LED_GPIO->PUPDR &= ~(BV(pin * 2 + 1) | BV(pin * 2));
	PERI_BB_ALS_WORD(LED_GPIO->PUPDR, pin * 2 + 1) = 0;
	PERI_BB_ALS_WORD(LED_GPIO->PUPDR, pin * 2) = 0;
}

void LedOn(uint32_t pin) {
	// write 1 to pin
	//LED_GPIO->ODR |= BV(pin);
	PERI_BB_ALS_WORD(LED_GPIO->ODR, pin) = 1;
}

void LedOff(uint32_t pin) {
	// write 0 to pin
	//LED_GPIO->ODR &= ~BV(pin);
	PERI_BB_ALS_WORD(LED_GPIO->ODR, pin) = 0;
}

void LedToggle(uint32_t pin) {
	// toggle the pin
	//LED_GPIO->ODR ^= BV(pin);
	PERI_BB_ALS_WORD(LED_GPIO->ODR, pin) ^= 1;
}

void LedBlink(uint32_t pin, uint32_t ms) {
	LedOn(pin);
	DelayMs(ms);
	LedOff(pin);
}




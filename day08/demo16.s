/* *************************************************************************
**  Target      : Cortex-M3
**  Environment : GNU Tools
**  Micro-Controller : LM3SXXXX *************************************************************************
*/
.syntax unified

.global pfnVectors
.global Default_Handler
.global SVC_Handler
.global _start
.equ Top_Of_Stack, 0x20002000    /* end of 8K RAM */

.global sdata
.global edata
.global etext
.global sbss
.global ebss

.section .vectors
vectors:
	.word Top_Of_Stack          @ msp = 0x20002000
	.word _start                @ Starting Program address
	.word Default_Handler	    @ NMI_Handler
	.word Default_Handler	    @ HardFault_Handler
	.word Default_Handler	    @ MemManage_Handler
	.word Default_Handler	    @ BusFault_Handler
	.word Default_Handler	    @ UsageFault_Handler
	.word 0                     @ 7
	.word 0                     @ To
	.word 0                     @ 10 
	.word 0                     @ Reserved
	.word Default_Handler	    	@ SVC_Handler
	.word Default_Handler	    @ DebugMon_Handler
	.word 0                     @ Reserved
	.word Default_Handler	    @ PendSV_Handler
	.word Default_Handler	    @ SysTick_Handler
	.word Default_Handler	    @ IRQ_Handler
	.word Default_Handler	    @ IRQ_Handler

.section .rodata


.section .data


.section .bss


.section .text
.thumb
/**
 * This is the code that gets called when the processor first
 * starts execution following a reset. 
*/
.type _start, %function
_start:                              @ _start label is required by the linker

	//init .data section
	ldr r7, =etext
	ldr r6, =sdata
	ldr r5, =edata
	mov r4, #0
data_init:
	cmp r6, r5
	beq data_init_end
	ldrb r4, [r7], #1
	strb r4, [r6], #1
	b data_init
data_init_end:

	//clear .bss section
	ldr r6, =sbss
	ldr r7, =ebss
	mov r4, #0
bss_init:
	cmp r6, r7
	beq bss_init_end
	strb r4, [r6], #1
	b bss_init
bss_init_end:

	bl main
	stop:   b stop

/***************************************************************/
.global main
.type main, %function
main:                              
	@ saturated math --> DSP instructions
	mov r0, #10
	usat r1, #5, r0, lsl #1		@ r1 = r0 * 2 = 20 (no sat q=0)
	usat r1, #5, r0, lsl #2		@ r1 = r0 * 4 = 40 --> 31 (sat q = 1)

	@ simd instruction --> multi-media
	@ldr r1, =0x11223344
	@ldr r2, =0x44332211
	@qadd8 r0, r1, r2

	@ rev instruction
	ldr r0, =0x11223344
	rev r1, r0					@ r1 = rev(r0) --> (CM3/4)LE <--> BE(Network)

	@ sign extend
	ldr r0, =0x55AA8765
	sxtb r1, r0
	sxth r2, r0
	uxth r3, r0

	@ bit-field extract
	ldr r6, =0x11223344
	@ get bit 4 to bit 12 in a register
		@ r0 = (r6 >> 4) & 0x0FFF
	ubfx r0, r6, #4, #12	@ get bit4-12 of r6 into r0 --> r0 = 0x334

	@ make bits [15:8] of r1 as 0x12
	ldr r1, =0x11223344
	bfc r1, #8, #16			@ r1 will be 0x11000044	@ r1 &= ~((0xFFFF) << 8)
	mov r0, 0x12
	bfi r1, r0, #8, #16		@ r1 will be 0x11001244 @ r1 |= ((uint16_t)r0 << 8)

	ldr r1, =0x12345678		@ r1 = 0001 0010 ............. 
	clz r0, r1				@ count leading zero bits --> 3

	mov r7, #5
	rbit r8, r7				@ reverse bits
							@ r1 = 11001010		--> r1 = 01010011
	mov pc, lr

/***************************************************************/

.type Default_Handler, %function
Default_Handler:
	halt: b halt

.end


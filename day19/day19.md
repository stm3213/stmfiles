# Micro-controllers - ARM

## Agenda
* CAN

## CAN Demo
* Clock Setup
	* HCLK = 72 MHz
	* APB1 PCLK = 36 MHz
* CAN1 Basic config
	* Activate
	* Select RX & TX pins: PB8, PB9
	* Prescalar = 18
	* CAN time quanta = PCLK / Prescalar
		* = 18 / 36 M = 500 nsec
	* Time Quatas
		* Bit Seg 1 -- 2 Quanta
		* Bit Seg 2 -- 1 Quanta
		* SJW Seg -- 1 Quanta
	* Bit timing = 4 Quantas * Time Qunta = 2000 ns.
	* Baud Rate = 1 / Bit timing = 1 / 2000 ns = 500000 (bits per sec)
	* Mode = Loopback mode
	* Interrupt Enable = RX0 interrupt -- RX0 FIFO not empty
* Acceptance Filter (After CAN init)


## FPGA
* Introduction to FPGA
* FPGA -- Field Programmable Gate Array.
* Logic gates
	* AND, OR, NOT, NAND, NOR.
* Any logic cct can be built using this e.g. Half Adder, Full Adder, Multiplier, Barrel shifter, UART, PWM, ...
* Digital lab assignment (1st year of BE)
	* Pre-requisite
		* Boolean expression
		* K-map
		* Gate logic diagram
	* Design a half adder
		* Logic gate ICs -- AND, OR, NOT, NAND, NOR.
* FPGA -- set of logic gates (in an IC) -- programmable connections.
	* Digital program --> Burn into FPGA --> Gate inter-connections will done accordingly.
	* Program -- Hardware Description Language -- VHDL or Verilog.
* FPGA vs Controllers
	* Controllers (general purpose) --> Programs to fulfil any functionality.
		* Program runs on processor/ALU/registers.
		* Consuming clock
		* Power consumpution
	* FPGA (dedicated as per need) --> Hardware -- logic gate connections.
		* Less power consumpution
		* Very fast
		* Programmable

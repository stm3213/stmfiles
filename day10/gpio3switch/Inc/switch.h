/*
 * switch.h
 *
 *  Created on: Jul 5, 2021
 *      Author: admin
 */

#ifndef SWITCH_H_
#define SWITCH_H_

#include "stm32f4xx.h"

#define SWITCH_GPIO			GPIOA
#define SWITCH_PIN			0

#define SWITCH_CLK_EN		0

void SwitchInit(void);
int SwitchGetState(void);
void SwitchWaitForPress(void);

#endif /* SWITCH_H_ */

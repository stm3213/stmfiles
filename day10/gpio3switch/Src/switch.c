/*
 * switch.c
 *
 *  Created on: Jul 5, 2021
 *      Author: admin
 */

#include "switch.h"

void SwitchInit(void) {
	// enable gpio A clock
	RCC->AHB1ENR |= BV(SWITCH_CLK_EN);

	// set switch pins as input pins [00]
	SWITCH_GPIO->MODER &= ~(BV(SWITCH_PIN * 2 + 1) | BV(SWITCH_PIN * 2));

	// set low speed [00]
	SWITCH_GPIO->OSPEEDR &= ~(BV(SWITCH_PIN * 2 + 1) | BV(SWITCH_PIN * 2));

	// configure no pull-up & pull-down resistor
	SWITCH_GPIO->PUPDR &= ~(BV(SWITCH_PIN * 2 + 1) | BV(SWITCH_PIN * 2));
}

int SwitchGetState(void) {
	// check SWITCH_PIN bit on SWITCH_GPIO->IDR
	if((SWITCH_GPIO->IDR & BV(SWITCH_PIN)) == 0)
		return 0; // OPEN
	return 1; // CLOSE
}

void SwitchWaitForPress(void){
	while(SwitchGetState() == 0)
		;
}



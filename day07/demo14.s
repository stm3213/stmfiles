/* *************************************************************************
**  Target      : Cortex-M3
**  Environment : GNU Tools
**  Micro-Controller : LM3SXXXX *************************************************************************
*/
.syntax unified

.global pfnVectors
.global Default_Handler
.global SVC_Handler
.global _start
.equ Top_Of_Stack, 0x20002000    /* end of 8K RAM */

.global sdata
.global edata
.global etext
.global sbss
.global ebss

.section .vectors
vectors:
	.word Top_Of_Stack          @ msp = 0x20002000
	.word _start                @ Starting Program address
	.word Default_Handler	    @ NMI_Handler
	.word Default_Handler	    @ HardFault_Handler
	.word Default_Handler	    @ MemManage_Handler
	.word Default_Handler	    @ BusFault_Handler
	.word Default_Handler	    @ UsageFault_Handler
	.word 0                     @ 7
	.word 0                     @ To
	.word 0                     @ 10 
	.word 0                     @ Reserved
	.word Default_Handler	    	@ SVC_Handler
	.word Default_Handler	    @ DebugMon_Handler
	.word 0                     @ Reserved
	.word Default_Handler	    @ PendSV_Handler
	.word Default_Handler	    @ SysTick_Handler
	.word Default_Handler	    @ IRQ_Handler
	.word Default_Handler	    @ IRQ_Handler

.section .rodata


.section .data


.section .bss


.section .text
.thumb
/**
 * This is the code that gets called when the processor first
 * starts execution following a reset. 
*/
.type _start, %function
_start:                              @ _start label is required by the linker

	//init .data section
	ldr r7, =etext
	ldr r6, =sdata
	ldr r5, =edata
	mov r4, #0
data_init:
	cmp r6, r5
	beq data_init_end
	ldrb r4, [r7], #1
	strb r4, [r6], #1
	b data_init
data_init_end:

	//clear .bss section
	ldr r6, =sbss
	ldr r7, =ebss
	mov r4, #0
bss_init:
	cmp r6, r7
	beq bss_init_end
	strb r4, [r6], #1
	b bss_init
bss_init_end:

	bl main
	stop:   b stop

/***************************************************************/
.global main
.type main, %function
main:  
	stmdb sp!, {lr}		@ take backup of lr into stack 

	@ r0 = add_two(10, 20)
	mov r0, #10
	mov r1, #20
	bl add_two
	
	@ r0 = add_three(10, 20, 30)
	push {r0-r3, r12}
	mov r0, #10
	mov r1, #20
	mov r2, #30
	bl add_three
	pop {r0-r3, r12}

	ldmia sp!, {lr}		@ restore lr from stack 
	mov pc, lr


.global add_two
.type add_two, %function
add_two: 
	stmfd sp!, {r4-r11,lr}		@ take backup of lr into stack                              
	@ arg1 -> r0, arg2 -> r1

	@ r0 = r0 + r1
	add r0, r1

	ldmfd sp!, {r4-r11,lr}		@ restore lr from stack 
	@ return value -> r0
	mov pc, lr

.global add_three
.type add_three, %function
add_three: 
	push {r4-r11,lr}		@ take backup of lr into stack                              
	@ arg1 -> r0, arg2 -> r1, arg3 -> r2

	mov r4, r0
	add r4, r1
	add r4, r2
	mov r0, r4

	pop {r4-r11,lr}		@ restore lr from stack 
	@ return value -> r0
	mov pc, lr

/***************************************************************/

.type Default_Handler, %function
Default_Handler:
	halt: b halt

.end


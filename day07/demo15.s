/* *************************************************************************
**  Target      : Cortex-M3
**  Environment : GNU Tools
**  Micro-Controller : LM3SXXXX *************************************************************************
*/
.syntax unified

.global pfnVectors
.global Default_Handler
.global SVC_Handler
.global _start
.equ Top_Of_Stack, 0x20002000    /* end of 8K RAM */

.global sdata
.global edata
.global etext
.global sbss
.global ebss

.section .vectors
vectors:
	.word Top_Of_Stack          @ msp = 0x20002000
	.word _start                @ Starting Program address
	.word Default_Handler	    @ NMI_Handler
	.word Default_Handler	    @ HardFault_Handler
	.word Default_Handler	    @ MemManage_Handler
	.word Default_Handler	    @ BusFault_Handler
	.word Default_Handler	    @ UsageFault_Handler
	.word 0                     @ 7
	.word 0                     @ To
	.word 0                     @ 10 
	.word 0                     @ Reserved
	.word SVC_Handler	    	@ SVC_Handler
	.word Default_Handler	    @ DebugMon_Handler
	.word 0                     @ Reserved
	.word Default_Handler	    @ PendSV_Handler
	.word Default_Handler       @ SysTick_Handler
	.word Default_Handler	    @ IRQ_Handler
	.word Default_Handler	    @ IRQ_Handler

.section .rodata


.section .data


.section .bss


.section .text
.thumb
/**
 * This is the code that gets called when the processor first
 * starts execution following a reset. 
*/
.type _start, %function
_start:                              @ _start label is required by the linker

	//init .data section
	ldr r7, =etext
	ldr r6, =sdata
	ldr r5, =edata
	mov r4, #0
data_init:
	cmp r6, r5
	beq data_init_end
	ldrb r4, [r7], #1
	strb r4, [r6], #1
	b data_init
data_init_end:

	//clear .bss section
	ldr r6, =sbss
	ldr r7, =ebss
	mov r4, #0
bss_init:
	cmp r6, r7
	beq bss_init_end
	strb r4, [r6], #1
	b bss_init
bss_init_end:

	bl main
	stop:   b stop

/***************************************************************/
.global main
.type main, %function
main:
	push {lr}

	mov r0, #23
	mov r1, #5
	svc #1				@ svc is thumb instruction

	mov r0, #23
	mov r1, #5
	svc #2				@ svc is thumb instruction

	mov r0, #23
	mov r1, #5
	svc #3				@ svc is thumb instruction

	pop {lr}
	mov pc, lr


.global SVC_Handler
.type SVC_Handler, %function
SVC_Handler:
	mrs r0, msp
	mov r8, r0
	ldr r0, [r0, #24] 
	ldrb r7, [r0, #-2]

	ldr r3, [r8, #12] 
	ldr r2, [r8, #8] 
	ldr r1, [r8, #4] 
	ldr r0, [r8, #0] 

	cmp r7, #1
	beq syscall_1
	cmp r7, #2
	beq syscall_2
	cmp r7, #3
	beq syscall_3

	bx lr			@ pc <-- lr


.global syscall_1
.type syscall_1, %function
syscall_1:

	add r0, r1
	str r0, [r8, #0] 

	bx lr			@ pc <-- lr (return to main)

.global syscall_2
.type syscall_2, %function
syscall_2:
	
	sub r0, r1
	str r0, [r8, #0]

	bx lr			@ pc <-- lr (return to main)
	
.global syscall_3
.type syscall_3, %function
syscall_3:

	mul r0, r1
	str r0, [r8, #0]

	bx lr			@ pc <-- lr (return to main)

/***************************************************************/

.type Default_Handler, %function
Default_Handler:
	halt: b halt

.end


/*
 * uart.c
 *
 *  Created on: 07-Jul-2021
 *      Author: Nilesh Ghule <nilesh@sunbeaminfo.com>
 */
#include <stdio.h>
#include "uart.h"

static char *tx_string = NULL;
static int tx_index = 0;
static volatile int tx_completed = 1;
static UartCallbackType uartCallback = NULL;

static void UartGpioInit(void) {
	// enable clock for GPIOA
	RCC->AHB1ENR |= BV(RCC_AHB1ENR_GPIOAEN_Pos);
	// set alt function for PA.2 & PA.3 as USART pins
	USART_AFR |= (GPIOA_USART_ALT_FN << GPIO_AFRL_AFSEL3_Pos) | (GPIOA_USART_ALT_FN << GPIO_AFRL_AFSEL2_Pos);
	// set gpio pin mode as Alt Fn [10]
	USART_GPIO->MODER &= ~(BV(USART_TX_PIN*2) | BV(USART_RX_PIN*2));
	USART_GPIO->MODER |= BV(USART_TX_PIN*2 + 1) | BV(USART_RX_PIN*2 + 1);
	// set speed of gpio pin [00]
	USART_GPIO->OSPEEDR &= ~(BV(USART_TX_PIN*2+1) | BV(USART_TX_PIN*2) | BV(USART_RX_PIN*2+1) | BV(USART_RX_PIN*2));
	// set push-mode output type [0]
	USART_GPIO->OTYPER &= ~(BV(USART_TX_PIN) | BV(USART_RX_PIN));
	// set no pull-up and pull-dwon resistor
	USART_GPIO->PUPDR &= ~(BV(USART_TX_PIN*2+1) | BV(USART_TX_PIN*2) | BV(USART_RX_PIN*2+1) | BV(USART_RX_PIN*2));
}

static void UartBusInit(uint32_t baud) {
	// enable usart2 clock
	RCC->APB1ENR |= BV(RCC_APB1ENR_USART2EN_Pos);
	// usart2 config -- control register (CR1, CR2 & CR3)
	// enable uart tx (TE=1), rx (RE=1), word len 8 (M=0), parity off (PCE=0), oversampling by 16 (OVER8=0)
	USART->CR1 = BV(USART_CR1_TE_Pos) | BV(USART_CR1_RE_Pos);
	// one stop bit(STOP=00), disable clock (uart not usart)
	USART->CR2 = 0;
	// no hardware flow control (RTSE=0, CTSE=0), no dma, no interrupts enabled
	USART->CR3 = 0;
	// configure baud rate
	if(baud == BAUD_9600)
		USART->BRR = 0x683;
	else if(baud == BAUD_38400)
		USART->BRR = 0x1A1;
	else if(baud == BAUD_115200)
		USART->BRR = 0x8B;
	// enable uart
	USART->CR1 |= BV(USART_CR1_UE_Pos);
}

void UartInit(uint32_t baud, UartCallbackType callback) {
	UartGpioInit();
	UartBusInit(baud);
	// store callback function address into global pointer
	uartCallback = callback;
	// enable uart interrpt in NVIC
	NVIC_EnableIRQ(USART_IRQn);
}

void UartPutch(int ch) {
	// wait for last char transmitted -- polling
	while((USART->SR & BV(USART_SR_TXE_Pos)) == 0);
	// write char into data register
	USART->DR = ch & 0x000000FF;
}

int UartGetch(void) {
	int ch;
	// wait for char to be received -- polling
	while((USART->SR & BV(USART_SR_RXNE_Pos)) == 0);
	// read char from data register
	ch = USART->DR;
	// return the character read
	return ch;
}

void UartPuts(char *str) {
	// wait if prev string is transmitting.
	while(tx_completed == 0);
	// mark starting of new transmission
	tx_completed = 0;
	// store addr of string to be transmitted in a global var, so that it can be accessed from ISR
	tx_string = str;
	// new string should be transmitted from 0th char
	tx_index = 0;
	// enable transmission interrupt
	USART->CR1 |= BV(USART_CR1_TXEIE_Pos);
}

void UartGets(char *str) {
	int i=0, ch;
	do {
		ch = UartGetch();
		str[i++] = ch;
	}while(ch != '\r');
	str[i++] = '\n';
	str[i] = '\0';
}

void USART2_IRQHandler(void) {
	uint32_t ch;
	// identify the interrupt
	// TXE interrupt -- if TXEIE=1 and TXE=1
	if((USART->CR1 & BV(USART_CR1_TXEIE_Pos)) && (USART->SR & BV(USART_SR_TXE_Pos))) {
		// send next char if available (not '\0') -- ack interrupt
		ch = tx_string[tx_index];
		if(ch != '\0') {
			USART->DR = ch;
			tx_index++;
		}
		// if string is completed
		else {
			// mark current transmission completed
			tx_completed = 1;
			tx_string = NULL;
			// disable the TXE interrupt
			USART->CR1 &= ~BV(USART_CR1_TXEIE_Pos);
		}
		// when UART interrupt is raised, call the callback.
		if(uartCallback != NULL)
			uartCallback(1);
	}
	else if((USART->CR1 & BV(USART_CR1_RXNEIE_Pos)) && (USART->SR & BV(USART_SR_RXNE_Pos))) {
		// receive next char

		// when UART interrupt is raised, call the callback.
		if(uartCallback != NULL)
			uartCallback(2);
	}
}






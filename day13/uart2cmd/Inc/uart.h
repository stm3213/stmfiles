/*
 * uart.h
 *
 *  Created on: 07-Jul-2021
 *      Author: Nilesh Ghule <nilesh@sunbeaminfo.com>
 */

#ifndef UART_H_
#define UART_H_

#include "stm32f4xx.h"

// USART2 --> APB1 (42 Mhz Max) --> PA.2 is TX (AF7), PA.3 is RX (AF7)
#define GPIOA_USART_ALT_FN	7
#define USART_AFR			GPIOA->AFR[0]

#define USART_GPIO			GPIOA
#define USART_TX_PIN		2
#define USART_RX_PIN		3

#define USART			USART2

#define BAUD_9600		9600
#define BAUD_38400		38400
#define BAUD_115200		115200

// as default setting, PCLK for APB1 & APB2 is set to Fcclk i.e. 16 MHz (No prescaling)
#define PCLK	SystemCoreClock

void UartInit(uint32_t baud);
void UartPutch(int ch);
int UartGetch(void);
void UartPuts(char *str);
void UartGets(char *str);


#endif /* UART_H_ */

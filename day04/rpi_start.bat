REM start QEMU with RPI image [Initial Boot]
qemu-system-arm -kernel kernel-qemu-4.4.34-jessie -cpu arm1176 -m 256 -M versatilepb -no-reboot -serial stdio -append "root=/dev/sda2 panic=1 rootfstype=ext4 rw init=/bin/bash" -drive "file=2017-03-02-raspbian-jessie-lite.img,index=0,media=disk,format=raw"

REM start QEMU with RPI image [Regular Boot]
REM qemu-system-arm -kernel kernel-qemu-4.4.34-jessie -cpu arm1176 -m 256 -M versatilepb -no-reboot -serial stdio -append "root=/dev/sda2 panic=1 rootfstype=ext4 rw" -drive "file=2017-03-02-raspbian-jessie-lite.img,index=0,media=disk,format=raw" -net nic -net user,hostfwd=tcp::2222-:22
